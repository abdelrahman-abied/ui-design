import 'package:flutter/material.dart';
import 'package:mywebsite/pages/contact_me_page/contact_me.dart';
import 'package:mywebsite/pages/home_page/home_website.dart';

class CustomDrawer extends StatelessWidget {
  final String selectedTitle;
  const CustomDrawer(this.selectedTitle);
  Widget _appbarButton(String title, VoidCallback onTap) {
    return FlatButton(
      height: 100,
      child: Text(
        title,
        style: TextStyle(color: Colors.white),
      ),
      color: title == selectedTitle ? Colors.red : null,
      onPressed: onTap,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF122636),
      child: Column(
        children: [
          _appbarButton("About", () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (BuildContext context) {
              return HomeWebsite();
            }));
          }),
          _appbarButton("Portifolio", () {}),
          _appbarButton("Contact", () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (BuildContext context) {
              return ContactMe();
            }));
          }),
        ],
      ),
    );
  }
}
