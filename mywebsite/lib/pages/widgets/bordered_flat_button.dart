import 'package:flutter/material.dart';

class BoderedFlatButton extends StatelessWidget {
  final String title;
  final double height;
  final double width;
  final VoidCallback onTap;
  const BoderedFlatButton(
      {Key key,
      @required this.title,
      @required this.height,
      @required this.width,
      @required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        height: height,
        width: width,
        child: FlatButton(
          child: Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          onPressed: onTap,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
              side: BorderSide(width: 2, color: Colors.white)),
        ),
      ),
    );
  }
}
