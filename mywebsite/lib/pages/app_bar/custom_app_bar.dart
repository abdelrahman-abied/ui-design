import 'package:flutter/material.dart';
import 'package:mywebsite/pages/contact_me_page/contact_me.dart';
import 'package:mywebsite/pages/home_page/home_website.dart';
import 'package:mywebsite/pages/widgets/bordered_flat_button.dart';

class CustomAppBar extends StatelessWidget {
  final String selectedTitle;
  const CustomAppBar(this.selectedTitle);

  Widget _appbarButton(String title, VoidCallback onTap) {
    return FlatButton(
      height: 100,
      child: Text(
        title,
        style: TextStyle(color: Colors.white),
      ),
      color: title ==selectedTitle? Colors.red : null,
      onPressed: onTap,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          SizedBox(
            height: 50,
          ),
          Image.asset('assets/images/abdelrahman.png'),
          Spacer(
            flex: 2,
          ),
          Row(
            children: [
            
              _appbarButton("About", () {
                     Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return HomeWebsite();
                }));
              }),
              _appbarButton("Portifolio", () {}),
              _appbarButton("Contact", () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ContactMe();
                }));
              }),
              BoderedFlatButton(
                title: "Get Started",
                height: 40,
                width: 150,
                onTap: () {},
              ),
            ],
          )
        ],
      ),
    );
  }
}
