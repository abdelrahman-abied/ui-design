import 'package:flutter/material.dart';
import 'package:contactus/contactus.dart';
import 'package:mywebsite/pages/app_bar/custom_app_bar.dart';

class ContactMe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFF122636),
     appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 70.0),
        child: CustomAppBar("Contact"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Center(
          child: ContactUs(
            
            cardColor: Color(0xFF122536),
            logo: AssetImage('assets/images/personal.jpg'),
            email: 'abied.abiad@gmail.com',
            companyName: 'Abdelrahman Abied',
            phoneNumber: '+0201145227769',
            githubUserName: 'abdelrahman-abied',
            linkedinURL: 'https://www.linkedin.com/in/abhishek-doshi-520983199/',
            tagLine: 'Mobile Developer',
            textColor: Colors.white,
            taglineColor: Color(0xFF122636),
            companyColor: Colors.white,
          ),
        ),
      ),
    );
  }
}
