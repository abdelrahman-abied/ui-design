import 'package:flutter/material.dart';
import 'package:mywebsite/pages/app_bar/custom_app_bar.dart';
import 'package:mywebsite/pages/home_page/content_about_me.dart';
class HomeWebsite extends StatelessWidget {
  const HomeWebsite({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
        final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFF122636),
      appBar: PreferredSize(
        preferredSize: Size(screenSize.width, 70.0),
        child: CustomAppBar("About"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ContentAboutMe(),
                  Card(
                    child: Image.asset(
                      'assets/images/programming.gif',
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}