import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'home_mobile.dart';
import 'home_website.dart';

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final screenSize = MediaQuery.of(context).size;

    return ScreenTypeLayout(
      mobile: HomeMobile(),
      desktop: HomeWebsite(),
    );
  }
}
