import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mywebsite/pages/widgets/bordered_flat_button.dart';
import 'package:url_launcher/url_launcher.dart';

class ContentAboutMe extends StatelessWidget {
  const ContentAboutMe({Key key}) : super(key: key);
  Widget _iconContact(IconData iconData, VoidCallback onTap) {
    return IconButton(
      onPressed: onTap,
      icon: Icon(iconData, color: Colors.white),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Hello I am',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
        Text(
          'Abdelrahman Abied',
          style: TextStyle(
            fontSize: 40,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            height: 2,
          ),
        ),
        Text(
          'Find Me On',
          style: TextStyle(
            fontSize: 25,
            color: Colors.grey,
            fontWeight: FontWeight.normal,
            height: 2,
          ),
        ),
        Row(
          children: [
            _iconContact(FontAwesomeIcons.facebook, () {
              _launchInBrowser("https://www.facebook.com/abied.abaid");
            }),
            _iconContact(FontAwesomeIcons.twitter, () {}),
            _iconContact(FontAwesomeIcons.linkedin, () {
              _launchInBrowser(
                  "https://www.linkedin.com/in/abd-el-rahman-mohammad-1583b712a/");
            }),
            _iconContact(FontAwesomeIcons.github, () {
              _launchInBrowser("https://github.com/abdelrahman-abied");
            }),
            _iconContact(FontAwesomeIcons.googlePlay, () {
              _launchInBrowser(
                  "https://play.google.com/store/apps/developer?id=Kira+LLC");
            })
          ],
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          children: [
            SizedBox(
              height: 40,
              width: 150,
              child: FlatButton(
                onPressed: () {},
                child: Text(
                  'Hire Me',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                color: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    24,
                  ),
                ),
              ),
            ),
            const SizedBox(width: 30),
            BoderedFlatButton(
              title: "Portfolio",
              height: 40,
              width: 150,
              onTap: () {},
            ),
          ],
        )
      ],
    );
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
