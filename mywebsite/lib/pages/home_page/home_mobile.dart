import 'package:flutter/material.dart';
import 'package:mywebsite/pages/home_page/content_about_me.dart';
import 'package:mywebsite/pages/widgets/drawer.dart';

class HomeMobile extends StatelessWidget {
  const HomeMobile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF122636),
      drawer: Drawer(
        child: CustomDrawer("About"),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Card(
                  child: Image.asset(
                    'assets/images/programming.gif',
                  ),
                ),
                ContentAboutMe(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
