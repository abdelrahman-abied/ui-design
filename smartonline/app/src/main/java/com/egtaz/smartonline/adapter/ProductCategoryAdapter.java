package com.egtaz.smartonline.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.egtaz.smartonline.ProductsActivity;
import com.egtaz.smartonline.R;
import com.egtaz.smartonline.app_navigator.AppNavigator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.ProductCategoryViewHolder> {

    private List<String> listItem = new ArrayList<>();
    AppNavigator navigator;

    public ProductCategoryAdapter(AppNavigator context, String[] names) {
        listItem = Arrays.asList(names);
        this.navigator = context;

    }


    @NonNull
    @Override
    public ProductCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_category_item_layout, parent, false);
        return new ProductCategoryViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ProductCategoryViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigator.moveToProductDetailsActivity(listItem.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void setListItem(List<String> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    public class ProductCategoryViewHolder extends RecyclerView.ViewHolder {
        TextView productCategoryName;

        public ProductCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            productCategoryName = itemView.findViewById(R.id.productCategoryName);
        }
    }

}
