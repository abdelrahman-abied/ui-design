package com.egtaz.smartonline.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.egtaz.smartonline.MainActivity;
import com.egtaz.smartonline.R;
import com.egtaz.smartonline.app_navigator.AppNavigator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoryRecyclerviewAdapter extends RecyclerView.Adapter<CategoryRecyclerviewAdapter.CategoryViewHolder> {

    private List<String> listItem = new ArrayList<>();
    private MainActivity activity;
    AppNavigator navigator;
    public CategoryRecyclerviewAdapter(AppNavigator navigator, String[] names) {
        this.navigator = navigator;
        this.listItem = Arrays.asList(names);
    }


    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item_layout, parent, false);
        return new CategoryViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
        holder.categoryName.setText(listItem.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigator.moveToProductActivity(listItem.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void setListItem(List<String> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryName = itemView.findViewById(R.id.categoryName);

        }
    }


}
