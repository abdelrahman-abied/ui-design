package com.egtaz.smartonline;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.egtaz.smartonline.adapter.ProductCategoryAdapter;
import com.egtaz.smartonline.app_navigator.AppNavigator;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductsActivity extends AppCompatActivity implements AppNavigator {
    private TextView categoryName;
    String title;
    private DrawerLayout drawer;
    RecyclerView productCategoryRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        title = getIntent().getStringExtra("categoryName");
        categoryName = findViewById(R.id.categoryTitle);
        categoryName.setText(title);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        findViewById(R.id.categoryDrawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawer.isDrawerOpen(Gravity.LEFT)) drawer.openDrawer(Gravity.LEFT);
                else drawer.closeDrawer(Gravity.RIGHT);
            }
        });

        productCategoryRecyclerView = findViewById(R.id.productCategoryRecyclerView);
        productCategoryRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        productCategoryRecyclerView.setHasFixedSize(true);
        String[] names = getResources().getStringArray(R.array.image_name);
        ProductCategoryAdapter adapter = new ProductCategoryAdapter(this, names);
        productCategoryRecyclerView.setAdapter(adapter);


        findViewById(R.id.basketImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.signInImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.chatImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.moreImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.searchImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });


    }


    @Override
    public void moveToProductActivity(String item) {

    }

    @Override
    public void moveToProductDetailsActivity(String item) {
        Intent intent = new Intent(ProductsActivity.this, ProductDetailsActivity.class);
        intent.putExtra("productName", item);
        startActivity(intent);
    }
}