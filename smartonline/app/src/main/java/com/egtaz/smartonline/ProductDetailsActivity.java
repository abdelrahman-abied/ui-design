package com.egtaz.smartonline;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.egtaz.smartonline.adapter.CategorySliderAdapter;

public class ProductDetailsActivity extends AppCompatActivity {
    private ViewPager mCategoryViewPager;
    private CategorySliderAdapter mCategorySliderAdapter;
    private ImageView[] mDots;
    private LinearLayout mDotLayout;
    final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    private String title;
    TextView productName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        lp.setMargins(5, 0, 2, 0);

        title = getIntent().getStringExtra("productName");
        productName=findViewById(R.id.productName);
        productName.setText(title);
        mCategoryViewPager = findViewById(R.id.productGalleryViewPager);
        mDotLayout = findViewById(R.id.galleryDotsLaypout);
        mCategorySliderAdapter = new CategorySliderAdapter(this);
        mCategoryViewPager.setAdapter(mCategorySliderAdapter);
        //   getSupportActionBar().setHomeButtonEnabled(true);
        //     getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //   getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
        addDotsIndcator(0);
        mCategoryViewPager.addOnPageChangeListener(viewListener);


        findViewById(R.id.basketImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.signInImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.chatImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.moreImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        findViewById(R.id.searchImageV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    public void addDotsIndcator(int position) {
        mDots = new ImageView[4];
        mDotLayout.removeAllViews();
        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new ImageView(this);
            mDots[i].setImageResource(R.drawable.dots);

            mDots[i].setImageResource(R.drawable.dots);
            mDots[i].setLayoutParams(lp);
            mDotLayout.addView(mDots[i]);
        }
        int i = mDots.length - 1;
        if (mDots.length > 0) {

            mDots[position].setImageResource(R.drawable.grey_dots);
            mDots[i].setLayoutParams(lp);

        }

    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndcator(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

}