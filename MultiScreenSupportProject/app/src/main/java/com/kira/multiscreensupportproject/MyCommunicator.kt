package com.kira.multiscreensupportproject

interface MyCommunicator { // Meant for inter-fragment communication

    fun displayDetails(title: String, description: String)
}