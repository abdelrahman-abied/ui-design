package com.kira.multiitemselectedrecyclerview;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {
    ArrayList<TextName> list = new ArrayList<>();
    MainActivity mainActivity;

    public Adapter(ArrayList<TextName> list, MainActivity mainActivity) {
        this.list = list;
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override

    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_view_layout, parent, false);

        return new MyViewHolder(view, mainActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.name.setText(list.get(position).getName());
        if (!mainActivity.isContexualModeEnabled) {
            holder.checkBox.setVisibility(View.GONE);
        } else {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(false);

        }

        if (!mainActivity.selectAll) {
            holder.checkBox.setChecked(false);
        } else {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(true);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();

    }

    public void removeItem(ArrayList<TextName> listOfSelectionNames) {
        for (int i = 0; i < listOfSelectionNames.size(); i++) {
            list.remove(listOfSelectionNames.get(i));
            notifyDataSetChanged();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        CheckBox checkBox;
        View view;

        public MyViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            name = itemView.findViewById(R.id.imageTitle);
            checkBox = itemView.findViewById(R.id.checkbox);
            view = itemView;
            view.setOnLongClickListener(mainActivity);
            checkBox.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
//            if (((CheckBox) v).isChecked()) {
//                Log.d("TAG", "onClick: abdo");
//                ((CheckBox) v).setChecked(false);
//                // هنا بقدر احدث الitem براحتى
//            }
            mainActivity.makeSelection(v, getAdapterPosition());
            name.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }
}
