package com.kira.multiitemselectedrecyclerview;

public class TextName {
private String name;

    public TextName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
