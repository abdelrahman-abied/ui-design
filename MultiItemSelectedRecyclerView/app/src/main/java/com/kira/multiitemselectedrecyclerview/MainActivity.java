package com.kira.multiitemselectedrecyclerview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements View.OnLongClickListener {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    String[] names;
    Adapter adapter;
    ArrayList<TextName> textNames;
    ArrayList<TextName> listOfSelectionNames;
    int counter = 0;
    boolean selectAll = false;
    TextView itemCounter;
    boolean isContexualModeEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        itemCounter = findViewById(R.id.itemCounter);
        itemCounter.setText("My App");
        listOfSelectionNames = new ArrayList<>();


        names = getResources().getStringArray(R.array.image_name);
        textNames = new ArrayList<>();

        for (int i = 0; i < names.length; i++) {
            textNames.add(new TextName(names[i]));
        }
        recyclerView = findViewById(R.id.recyclerView);
        adapter = new Adapter(textNames, MainActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            //when DisplayHomeAsUpEnabled is enabled
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normal_menu, menu);
        return true;
    }

    @SuppressLint("NewApi")
    @Override
    public boolean onLongClick(View v) {
        isContexualModeEnabled = true;
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.contexual_menu);
        toolbar.setBackgroundColor(getColor(R.color.colorAccent));
        adapter.notifyDataSetChanged();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    public void makeSelection(View v, int adapterPosition) {
        if (((CheckBox) v).isChecked()) {
            listOfSelectionNames.add(textNames.get(adapterPosition));
            counter++;
        } else {
            listOfSelectionNames.remove(textNames.get(adapterPosition));
            counter--;
        }
        updateCounter();
    }

    public void updateCounter() {
        itemCounter.setText(counter + " Ttem Selected");
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.delete) {
            if (listOfSelectionNames.isEmpty()) {
                Toast.makeText(this, "No items selected for delete it", Toast.LENGTH_SHORT).show();
            } else {
                adapter.removeItem(listOfSelectionNames);
                removeContexualActionModeBar();
            }
        } else if (item.getItemId() == android.R.id.home) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            adapter.notifyDataSetChanged();
        } else if (item.getItemId() == R.id.selectAll) {
            Log.d("TAG", "onOptionsItemSelected: abdo");
            if (selectAll == false) {
                selectAll = true;
            } else {
                selectAll = false;
            }

            Log.d("TAG", "onOptionsItemSelected: abdo" + selectAll);

            adapter.notifyDataSetChanged();
        }

        return true;
    }

    @SuppressLint("NewApi")
    private void removeContexualActionModeBar() {
        isContexualModeEnabled = false;
        itemCounter.setText("My App");
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.normal_menu);
        counter = 0;
        toolbar.setBackgroundColor(getColor(R.color.colorPrimary));
        listOfSelectionNames.clear();
        adapter.notifyDataSetChanged();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    @Override
    public void onBackPressed() {

        if (isContexualModeEnabled) {
            removeContexualActionModeBar();
        } else {
            super.onBackPressed();
        }
    }
}