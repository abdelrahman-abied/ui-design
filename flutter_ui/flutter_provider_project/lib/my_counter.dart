import 'package:flutter/widgets.dart';

class MyCounter extends ChangeNotifier {
  int counter = 0;
  increment() {
    counter++;
    notifyListeners();
  }
}
