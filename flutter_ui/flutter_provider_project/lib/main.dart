import 'package:flutter/material.dart';
import 'package:flutter_provider_project/my_counter.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MyCounter>(
      create: (context) => MyCounter(),
      child: Scaffold(
          appBar: AppBar(
            title: Consumer<MyCounter>(builder: (context, myCounter, child) {
              return Text(
                '${myCounter.counter}',
                style: TextStyle(fontWeight: FontWeight.w600),
              );
            }),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'You have pushed the button this many times:',
                ),
                Consumer<MyCounter>(builder: (context, myCounter, child) {
                  return Text(
                    '${myCounter.counter}',
                    style: TextStyle(fontWeight: FontWeight.w600),
                  );
                }),
                Consumer<MyCounter>(builder: (context, myCounter, child) {
                  return IconButton(
                    onPressed: () {
                      myCounter.increment();
                    },
                    icon: Icon(Icons.add),
                  );
                })
              ],
            ),
          ),
          floatingActionButton: Consumer<MyCounter>(builder: (context, myCounter, child) {
            return FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                myCounter.increment();
              },
            );
          })),
    );
  }
}
