import 'package:equatable/equatable.dart';

 class TimerState extends Equatable {
  final int duration;
  const TimerState(this.duration);

  @override
  List<Object> get props => [duration];
}
class Ready extends TimerState{
 const Ready(int duration) : super(duration);

}
class Pause extends TimerState{
const Pause(int duration) : super(duration);
}
class Running extends TimerState{
  const Running(int duration) : super(duration);
}
class Finised extends TimerState{
  const Finised() : super(0);
}
