import 'dart:convert';

import 'package:flutter_http_api/data/model/post.dart';
import 'package:http/http.dart';

abstract class PostRepository {
  Future<List<Post>> getAllPosts();
}

class PostRepositoryImpl extends PostRepository {
  @override
  Future<List<Post>> getAllPosts() async {
    final response = await get('http://jsonplaceholder.typicode.com/posts');
    if (response.statusCode == 200) {
      final parsed = json.decode(response.body).cast<Map<String, dynamic>>();
      return parsed.map<Post>((item) => Post.fromJson(item)).toList();
    } else {
      throw Exception('no data available');
    }
  }
}
