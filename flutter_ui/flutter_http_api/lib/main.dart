import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_http_api/bloc/post_bloc.dart';
import 'package:flutter_http_api/data/model/post.dart';
import 'package:flutter_http_api/data/repository/post_repository.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('http package'),
      ),
      body: BlocProvider(
        child: Home(),
        create: (context) => PostBloc(PostRepositoryImpl())..add(LoadPosts()),
      ),
    );
  }

  Future<Post> getPostById() async {
    http.Response post =
        await http.get('http://jsonplaceholder.typicode.com/posts/1');
    if (post.statusCode == 200) {
      print(post.body);
      return Post.fromJson(json.decode(post.body));
    } else {
      throw Exception("can't load data");
    }
  }
}
