import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_http_api/bloc/post_bloc.dart';

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocBuilder<PostBloc, PostState>(
          builder: (context, state) {
            if (state is PostLoading)
              return CircularProgressIndicator();
            else if (state is PostLoaded) {
              return ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(
                      state.posts[index].title,
                    ),
                  );
                },
                itemCount: state.posts.length,
              );
            } else if (state is PostError) {
              return Text(' Error Happened');
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
